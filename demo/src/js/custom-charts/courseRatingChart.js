import * as d3 from "d3";
import { BaseChartWidget } from "@polaris/dashboard-sdk/dist/bundle";

export class CourseRatingChart extends BaseChartWidget {
  constructor(title = "", description, data, options = {}) {
    if (options.transform) data = (data ?? []).map(options.transform);

    super(title, description, data, options);
  }

  plot(divWidth, divHeight) {
    const [width, height] = this.clearAndScaleSvg(divWidth - 100, divHeight, 200);
    this.drawTitle();

    if (!this.dataIsValid) {
      this.showErrorMessage(divWidth, divHeight);
      return this.wrapper.innerHTML;
    }

    const xScale = d3.scaleBand().domain([1, 2, 3, 4, 5]).range([0, width]);

    const xAxis = d3.axisBottom(xScale).ticks(5);

    this.g
      .append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

    // xAxis labels
    this.g
      .append("g")
      .append("text")
      .attr("x", width)
      .attr("y", height + 50)
      .attr("fill", "black")
      .text(this.options.xAxisLabel);
    this.appendYAxisLabel();

    const yScale = d3
      .scaleBand()
      .domain(this.data.map((d) => d.column1))
      .range([0, height]);

    this.g.append("g").call(d3.axisLeft(yScale));

    const color = d3.scaleOrdinal().range(this.colorRange);

    const nodes = this.g.selectAll(".bar").data(this.data).enter().append("g");

    nodes
      .append("circle")
      .attr("r", 20)
      .attr("fill", (d) => color(d.column2))
      .attr("stroke", "grey")
      .attr("cx", (d) => xScale(d.column2) + xScale.bandwidth() / 2)
      .attr("cy", (d) => yScale(d.column1) + yScale.bandwidth() / 2);

    nodes
      .append("circle")
      .attr("r", 2)
      .attr("cx", (d) => xScale(d.column2) + xScale.bandwidth() / 2 - 7)
      .attr("cy", (d) => yScale(d.column1) + yScale.bandwidth() / 2 - 5)
      .text((d) => d.column2);

    nodes
      .append("circle")
      .attr("r", 2)
      .attr("cx", (d) => xScale(d.column2) + xScale.bandwidth() / 2 + 7)
      .attr("cy", (d) => yScale(d.column1) + yScale.bandwidth() / 2 - 5)
      .text((d) => d.column2);

    nodes
      .append("circle")
      .attr("r", 2)
      .attr("cx", (d) => xScale(d.column2) + xScale.bandwidth() / 2 + 7)
      .attr("cy", (d) => yScale(d.column1) + yScale.bandwidth() / 2 - 5)
      .text((d) => d.column2);

    nodes
      .append("g")
      .attr("transform", function (d) {
        const x = xScale(d.column2) + xScale.bandwidth() / 2 - 5;
        const y = yScale(d.column1) + yScale.bandwidth() / 2 + 5;
        return `translate(${x}, ${y})`;
      })
      .append("path")
      .attr("d", (d) => {
        switch (d.column2) {
          case 1:
            return d3.line()([
              [0, 0],
              [5, 10],
              [10, 0],
            ]);
          case 2:
            return d3.line()([
              [0, 0],
              [5, 5],
              [10, 0],
            ]);
          case 3:
            return d3.line()([
              [0, 0],
              [5, 0],
              [10, 0],
            ]);
          case 4:
            return d3.line()([
              [0, 0],
              [5, -5],
              [10, 0],
            ]);
          case 5:
            return d3.line()([
              [0, 0],
              [5, -10],
              [10, 0],
            ]);
        }
      })
      .attr("stroke", "black")
      .attr("marker-start", "url(#dot)")
      .attr("fill", "none");

    return this.wrapper.innerHTML;
  }

  drawShape(placeholder) {
    placeholder.append("circle").attr("r", 5).attr("stroke", "black").attr("fill", "#69a3b2");
  }
}
