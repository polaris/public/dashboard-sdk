import { GridWidget } from "@polaris/dashboard-sdk";
import {TextElement} from "../../../../src/js/elements/textelement";

const data = [
    {
        type: "textElement",
        name: "Block 1",
        options: {
            showLegend: false,
        },
        data: {
            text: "00.00.00 - noch ... Tage"
        }
    },
    {
        type: "textElement",
        name: "Block 2",
        options: {
            showLegend: false,
        },
        data: {
            text: "00.00.00 - noch ... Tage"
        }
    },
    {
        type: "textElement",
        name: "Block 3",
        options: {
            showLegend: false,
        },
        data: {
            text: "00.00.00 - noch ... Tage"
        }
    },
]


export const deadlinesGridDef = { "deadlines-grid-widget" :
        new GridWidget(
            "Kursvergleich",
            "Vergleich mit anderen Studierenden",
            data,
            {
                direction: "column"
            })
}
