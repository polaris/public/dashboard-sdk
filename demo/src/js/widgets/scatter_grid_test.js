import { GridWidget } from "@polaris/dashboard-sdk";

const data = [
    {
        type: "scatterchart",
        name: "0-25%",
        options: {
            showLegend: false,
            showAxes: false,
            xmax: 10,
            ymax: 100
        },
        data:
        [
            {column1: 0, column2: 40, highlight: false},
            {column1: 2, column2: 26, highlight: false},
            {column1: 5, column2: 28, highlight: false},
            {column1: 7, column2: 17, highlight: false},
            {column1: 9, column2: 39, highlight: false},
            {column1: 10, column2: 60, highlight: false},
            {column1: 8, column2: 78, highlight: false},
            {column1: 6, column2: 15, highlight: false},
            {column1: 4, column2: 4, highlight: false},
        ]
    },
    {
        type: "scatterchart",
        name: "26-50%",
        options: {
            showLegend: false,
            showAxes: false,
            xmax: 10,
            ymax: 100
        },
        data:
        [
            {column1: 1, column2: 42, highlight: false},
            {column1: 2, column2: 54, highlight: false},
            {column1: 3, column2: 87, highlight: false},
            {column1: 4, column2: 65, highlight: false},
            {column1: 5, column2: 78, highlight: false},
            {column1: 6, column2: 92, highlight: false},
            {column1: 7, column2: 12, highlight: false},
            {column1: 8, column2: 54, highlight: false},
            {column1: 9, column2: 34, highlight: false},
            {column1: 10, column2: 42, highlight: false},
            {column1: 1, column2: 27, highlight: false},
            {column1: 2, column2: 29, highlight: false},
            {column1: 3, column2: 11, highlight: false},
            {column1: 4, column2: 37, highlight: false},
            {column1: 5, column2: 10, highlight: false},
            {column1: 6, column2: 18, highlight: false},
            {column1: 7, column2: 95, highlight: false},
            {column1: 8, column2: 2, highlight: false},
        ]
    },
    {
        type: "scatterchart",
        name: "51-75%",
        options: {
            showLegend: false,
            showAxes: false,
            xmax: 10,
            ymax: 100
        },
        data:
        [
            {column1: 1, column2: 32, highlight: false},
            {column1: 2, column2: 34, highlight: false},
            {column1: 3, column2: 27, highlight: false},
            {column1: 4, column2: 45, highlight: false},
            {column1: 5, column2: 58, highlight: false},
            {column1: 6, column2: 72, highlight: false},
            {column1: 7, column2: 87, highlight: false},
            {column1: 8, column2: 80, highlight: false},
            {column1: 9, column2: 18, highlight: false},
            {column1: 10, column2: 25, highlight: false},
            {column1: 1, column2: 34, highlight: false},
            {column1: 2, column2: 46, highlight: false},
            {column1: 3, column2: 58, highlight: false},
            {column1: 4, column2: 37, highlight: false},
            {column1: 5, column2: 9, highlight: false},
            {column1: 6, column2: 10, highlight: false},
        ]
    },
    {
        type: "scatterchart",
        name: "76-100%",
        options: {
            showLegend: false,
            showAxes: false,
            xmax: 10,
            ymax: 100
        },
        data:
            [
                {column1: 1, column2: 32, highlight: false},
                {column1: 2, column2: 34, highlight: false},
                {column1: 3, column2: 27, highlight: false},
                {column1: 4, column2: 75, highlight: false},
                {column1: 5, column2: 98, highlight: false},
                {column1: 6, column2: 62, highlight: false},
                {column1: 7, column2: 72, highlight: false},
                {column1: 8, column2: 24, highlight: false},
                {column1: 9, column2: 34, highlight: true},
                {column1: 10, column2: 44, highlight: false},
                {column1: 1, column2: 67, highlight: false},
                {column1: 2, column2: 89, highlight: false},
                {column1: 3, column2: 21, highlight: false},
                {column1: 4, column2: 17, highlight: false},
                {column1: 5, column2: 80, highlight: false},
                {column1: 6, column2: 68, highlight: false},
                {column1: 7, column2: 45, highlight: false},
                {column1: 8, column2: 44, highlight: false},
                {column1: 9, column2: 26, highlight: false},
                {column1: 10, column2: 28, highlight: false},
                {column1: 1, column2: 17, highlight: false},
                {column1: 2, column2: 9, highlight: false},
                {column1: 3, column2: 6, highlight: false},
            ]
    }
]


export const scatterGridDef = { "scatter-grid-widget" :
    new GridWidget(
        "Kursvergleich",
        "Vergleich mit anderen Studierenden",
        data,
        {
            direction: "row"
        })
}
