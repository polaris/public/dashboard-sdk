import { HeatMapWidget } from "@polaris/dashboard-sdk";


const data = [
    {
        group: "A",
        variable: "v1",
        value: 12
    },
    {
        group: "A",
        variable: "v2",
        value: 1
    },
    {
        group: "A",
        variable: "v3",
        value: 26
    },
    {
        group: "A",
        variable: "v4",
        value: 86
    },
    {
        group: "A",
        variable: "v5",
        value: 10
    },
    {
        group: "B",
        variable: "v1",
        value: 82
    },
    {
        group: "B",
        variable: "v2",
        value: 65
    },
    {
        group: "B",
        variable: "v3",
        value: 51
    },
    {
        group: "B",
        variable: "v4",
        value: 2
    },
    {
        group: "B",
        variable: "v5",
        value: 15
    },
    {
        group: "C",
        variable: "v1",
        value: 23
    },
    {
        group: "C",
        variable: "v2",
        value: 35
    },
    {
        group: "C",
        variable: "v3",
        value: 45
    },
    {
        group: "C",
        variable: "v4",
        value: 36
    },
    {
        group: "C",
        variable: "v5",
        value: 19
    },
    {
        group: "D",
        variable: "v1",
        value: 89
    },
    {
        group: "D",
        variable: "v2",
        value: 64
    },
    {
        group: "D",
        variable: "v3",
        value: 4
    },
    {
        group: "D",
        variable: "v4",
        value: 39
    },
    {
        group: "D",
        variable: "v5",
        value: 75
    },
]

export const heatMapDef = { "heatmap-widget" :
    new HeatMapWidget(
        "Kursnutzung mit einer HeatMap",
        "Beispielhafte Kursnutzung mit einer Heatmap",
        data,
        {
            showLegend: false,
            xAxisLabel: "Days",
            yAxisLabel: "Students"
        })
}
