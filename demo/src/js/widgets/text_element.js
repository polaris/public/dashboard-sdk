import { GridWidget } from "@polaris/dashboard-sdk";
import {TextElement} from "@polaris/dashboard-sdk";

const data = [
    {
        type: "heatmap",
        name: "Block 1",
        options: {
            showLegend: false,
        },
        data:
        [

        ]
    },
    {
        type: "heatmap",
        name: "Block 2",
        options: {
            showLegend: false,
        },
        data:
        [

        ]
    },
    {
        type: "heatmap",
        name: "Block 3",
        options: {
            showLegend: false,
        },
        data:
        [

        ]
    }
]


export const textGridDef = { "text-grid-widget" :
    new TextElement(
        "00.00.00 - noch ... Tage",
        {
            direction: "row"
        })
}
