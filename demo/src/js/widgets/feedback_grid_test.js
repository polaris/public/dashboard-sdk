import { GridWidget } from "@polaris/dashboard-sdk";

const data = [
    {
        type: "textAreaElement",
        name: "",
        options: {
            showLegend: false,
        },
        data: {
            text: "Gib hier dein Feedback ein",
        }
    }
]


export const feedbackGridDef = { "feedback-grid-widget" :
    new GridWidget(
        "Feedback",
        "Feedback Element",
        data,
        {
            direction: "column"
        })
}
