import * as d3 from "d3";
import { BarChartWidget, ChartJSWidget } from "@polaris/dashboard-sdk";

const dict = {
    "accessed": 98,
    "started": 7,
    "graded": 7,
    "submitted": 7,
    "answered": 38,
    "leftUnanswered": 4
}

const data = Object.keys(dict).map(key => ({column1: key, column2: dict[key]}));

export const barChartDef = { "barchart-widget" :
    new BarChartWidget(
        "A sample bar chart",
        "sample description",
        data,
        {
            showLegend: true,
            xAxisLabel: "Status",
            yAxisLabel: "Count"
        })
}


const chartjs_data = {datasets: [{data: Object.keys(dict).map(key => dict[key])}], labels: Object.keys(dict)};

export const chartJSBarChartDef = { "chartjs-barchart-widget" :
    new ChartJSWidget(
        "A sample ChartJS bar chart",
        "sample description",
        chartjs_data,
        {
            chartjs_options: {
                scales: {
                    x: {
                        title: {
                            display: true,
                            text: "Status"
                        }
                    },
                    y: {
                        title: {
                            display: true,
                            text: "Count"
                        }
                    }
                }
            }
        }
    )
}
