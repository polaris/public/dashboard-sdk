import { GridWidget } from "@polaris/dashboard-sdk";

const data = [
    {
        type: "heatmap",
        name: "Block 1",
        options: {
            showLegend: false,
        },
        data:
        [

        ]
    },
    {
        type: "heatmap",
        name: "Block 2",
        options: {
            showLegend: false,
        },
        data:
        [

        ]
    },
    {
        type: "heatmap",
        name: "Block 3",
        options: {
            showLegend: false,
        },
        data:
        [

        ]
    }
]


export const mcTestGridDef = { "mc-test-grid-widget" :
    new GridWidget(
        "MC-Tests",
        "Vergleich von Multiple-Choice Elementen",
        data,
        {
            direction: "row"
        })
}
