import * as d3 from "d3";

const COLOR_RANGE_1 = [
  "gold",
  "blue",
  "green",
  "yellow",
  "black",
  "grey",
  "darkgreen",
  "pink",
  "brown",
  "slateblue",
  "grey1",
  "orange",
];
const COLOR_RANGE_2 = [
  "#1f77b4",
  "#aec7e8",
  "#ff7f0e",
  "#ffbb78",
  "#2ca02c",
  "#98df8a",
  "#d62728",
  "#ff9896",
  "#9467bd",
  "#c5b0d5",
  "#e377c2",
  "#f7b6d2",
  "#7f7f7f",
  "#c7c7c7",
  "#bcbd22",
  "#dbdb8d",
  "#17becf",
  "#9edae5",
  "salmon",
  "lightsalmon",
  "lightsteelblue",
  "steelblue",
  "yellow",
  "orange",
  "#cccccc",
  "#dddddd",
  "#eee",
  "#aaa",
  "#123456",
  "black",
];

export class BaseChartWidget {
  marginRight = 150;
  marginBottom = 150;
  colorRange = COLOR_RANGE_2;

  constructor(title, description, data, options = {}) {
    this.title = title;
    this.description = description;
    this.data = data;
    this.options = options;
    this.dataIsValid = Array.isArray(data) ? data.length > 0 : !!data;

    this.wrapper = document.createElement("div");
    this.svg = d3.select(this.wrapper).append("svg");
  }

  clearAndScaleSvg(divWidth, divHeight, translateX = null, translateY = null) {
    if (this.g) this.g.remove();
    this.svg.selectAll("*").remove();

    const x = translateX ? translateX : 100;
    const y = translateY ? translateY : 50;

    const bg_translate = this.title ? [5, 35] : [0, 0];
    const bg = this.svg.append("rect")
        .attr("width", this.title ? divWidth - 10 : divWidth)
        .attr("height", this.title ? divHeight - 40 : divHeight)
        .attr("fill", "white")
        .attr("rx", 3)
        .attr("ry", 3)
        .attr("transform", `translate(${bg_translate[0]},${bg_translate[1]})`);
    this.g = this.svg.append("g").attr("transform", `translate(${x},${y})`);

    this.svg.attr("height", divHeight).attr("width", divWidth);
    if(!this.options?.disableBackground)
        this.svg.attr("style", "background-color: #f5f5f5");
    const width = this.svg.attr("width") - this.marginRight;
    const height = this.svg.attr("height") - this.marginBottom;

    return [width, height];
  }

  drawTitle() {
    if (this.title)
      this.svg
        .append("g")
        .attr("transform", "translate(10, 0)")
        .append("text")
        .attr("text-anchor", "start")
        .attr("x", 0)
        .attr("y", 25)
        .attr("class", "chart-title")
        .text(this.title);

    if (this.description && !this.options?.disableQuestionMark) {
      const randomId = Date.now();
      this.svg
        .append("g")
        .attr("transform", `translate(${this.svg.attr("width") - 40}, 5)`)
        .append("path")
        .attr("class", "question-mark")
        .attr("id", `id-${randomId}`)
        // Font Awesome Pro 5.15.4 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License)
        .attr(
          "d",
          "M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z"
        );

      setTimeout(() => {
        const targetEl = document.getElementById(`id-${randomId}`);
        if (targetEl)
          targetEl.addEventListener("click", (event) => {
            this.options.onShowDesc(this.description);
          });
      });
    }
  }

  appendXAxis(x, height) {
    const xAxis = this.options.ticks
      ? d3.axisBottom(x).ticks(this.options.ticks).tickFormat(d3.timeFormat("%d.%m.%Y %H:%M"))
      : d3.axisBottom(x).tickFormat(d3.timeFormat("%d.%m.%Y %H:%M"));

    this.g
      .append("g")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);
  }

  appendXAxisLabel(width, height) {
    if (this.options.xAxisLabel)
      this.g
        .append("g")
        .append("text")
        .attr("x", width)
        .attr("y", height + 50)
        .attr("text-anchor", "end")
        .attr("font-size", "18px")
        .attr("fill", "black")
        .text(this.options.xAxisLabel);
  }

  appendYAxisLabel() {
    if (this.options.yAxisLabel)
      this.g
        .append("g")
        .append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 6)
        .attr("dy", "-4em")
        .attr("text-anchor", "end")
        .attr("font-size", "18px")
        .attr("fill", "black")
        .text(this.options.yAxisLabel);
  }

  showErrorMessage(divWidth, divHeight) {
    this.g
      .append("g")
      .append("text")
      .attr("x", divWidth / 2 - 140)
      .attr("y", divHeight / 2 - 20)
      .attr("class", "chart-error-message")
      .text("No data available.");
  }

  showLegend(keys, color, divWidth) {
    this.svg
      .append("g")
      .selectAll("g")
      .data(keys)
      .enter()
      .append("circle")
      .attr("cx", divWidth - 120)
      .attr("cy", function (d, i) {
        return 25 + i * 25;
      })
      .attr("r", 7)
      .style("fill", function (d) {
        return color(d);
      });

    this.svg
      .selectAll("label")
      .data(keys)
      .enter()
      .append("text")
      .attr("x", divWidth - 100)
      .attr("y", function (d, i) {
        return 25 + i * 25;
      })
      .style("fill", function (d) {
        return color(d);
      })
      .text(function (d) {
        return d;
      })
      .attr("text-anchor", "left")
      .style("alignment-baseline", "middle");
  }
}
