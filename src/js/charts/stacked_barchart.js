import * as d3 from "d3";
import { BaseChartWidget } from "./base";

export class StackedBarChartWidget extends BaseChartWidget {
  constructor(title, description, data, groups, options) {
    if (options.transform) data = (data ?? []).map(options.transform);

    super(title, description, data, options);
    this.groups = groups;
  }

  plot(divWidth, divHeight, el) {
    const [width, height] = this.clearAndScaleSvg(divWidth, divHeight);
    this.drawTitle();

    if (!this.dataIsValid) {
      this.showErrorMessage(divWidth, divHeight);
      return this.wrapper.innerHTML;
    }

    const subgroups = Object.keys(this.data["overall"][0]);
    const groups = Object.keys(this.data);

    const data = Object.keys(this.data).reduce((acc, key) => [...acc, { ...this.data[key][0], group: key }], []);

    const stackedData = d3.stack().keys(subgroups)(data);

    const min = stackedData[0][0][0];
    const max = stackedData[stackedData.length - 1][0][1];

    const x = d3.scaleBand().domain(groups).range([0, width]).padding([0.2]);
    this.g.append("g").attr("transform", `translate(0, ${height})`).call(d3.axisBottom(x).tickSizeOuter(0));

    const y = d3.scaleLinear().domain([min, max]).range([height, 0]);
    this.g.append("g").call(d3.axisLeft(y));

    const color = d3.scaleOrdinal().domain(["overall"]).range(this.colorRange);

    this.g
      .append("g")
      .selectAll("g")
      .data(stackedData)
      .join("g")
      .attr("fill", (d) => color(d.key))
      .selectAll("rect")
      .data((d) => d)
      .join("rect")
      .attr("x", (d) => {
        return x(d.data.group);
      })
      .attr("x", (d) => x(d.data.group))
      .attr("y", (d) => y(d[1]))
      .attr("height", (d) => y(d[0]) - y(d[1]))
      .attr("width", x.bandwidth());

    if (this.options.showLegend) this.showLegend(subgroups, color, divWidth);

    return this.wrapper.innerHTML;
  }
}
