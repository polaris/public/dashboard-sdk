import * as d3 from "d3";
import {BaseChartWidget} from "./base";

// Copyright 2021 Observable, Inc.
// Released under the ISC license.
// https://observablehq.com/@d3/grouped-bar-chart


export class SimpleGroupedBarChartWidget extends BaseChartWidget {

  constructor(titleText, description, dict, {
    title, // given d in data, returns the title text
    marginTop = 0, // top margin, in pixels
    marginRight = 0, // right margin, in pixels
    marginBottom = 0, // bottom margin, in pixels
    marginLeft = 40, // left margin, in pixels
    width = 640, // outer width, in pixels
    height = 400, // outer height, in pixels
    xDomain, // array of x-values
    xRange = [marginLeft, width - marginRight], // [xmin, xmax]
    xPadding = 0.1, // amount of x-range to reserve to separate groups
    yType = d3.scaleLinear, // type of y-scale
    yDomain, // [ymin, ymax]
    yRange = [height - marginBottom, marginTop], // [ymin, ymax]
    zDomain, // array of z-values
    zPadding = 0.05, // amount of x-range to reserve to separate bars
    yFormat, // a format specifier string for the y-axis
    yLabel, // a label for the y-axis
    colors = d3.schemeTableau10, // array of colors
    drawLegend = true
  } = {}) {

    const data = Object.keys(dict).reduce( (agg, key) => [...agg, {...dict[key], name : key}], [])
    const categories = Array.from( new Set(
        data.flatMap(
              item => Object.keys(item)
                )
        )).filter(s => s != "name");

    const names = data.reduce( (agg, curr) => [...agg, curr["name"]], [])
    const dataFlatted = categories.flatMap(category => data.map(d => ({name : d.name, category : category, value: d[category]}))) // pivot longer

    const options = {
      x: d => d.name,
      y: d => d.value,
      z: d => d.category,
      title,
      marginTop,
      marginRight,
      marginBottom,
      marginLeft,
      width,
      height,
      xRange,
      xPadding,
      yType,
      yDomain,
      yRange,
      zDomain,
      zPadding,
      yFormat, // a format specifier string for the y-axis
      yLabel, // a label for the y-axis
      drawLegend,      
      xDomain: names,
      //yDomain : [0,40],
      zDomain: categories,
      colors: d3.schemeSpectral[categories.length < 10 ? 10 : categories.length]
    }
    super(title, description, dataFlatted, options);

    this.options = options
    this.dataFlatted =dataFlatted

    this.wrapper = document.createElement("div");
    this.svg = d3.select(this.wrapper).append("svg");
  }

  plot(divWidth, divHeight, el) {
    let [w, h] = this.clearAndScaleSvg(divWidth, divHeight);
   // this.drawTitle();

    let {
      x,
      y,
      z,
      title,
      marginTop,
      marginRight,
      marginBottom,
      marginLeft,
      width,
      height,
      xDomain,
      xRange,
      xPadding,
      yType,
      yDomain,
      yRange,
      zDomain,
      zPadding,
      yFormat, // a format specifier string for the y-axis
      yLabel, // a label for the y-axis
      colors,
      drawLegend
    } = this.options

    const labelOffset = 15

     width = w
     height = h + 150
     marginTop = labelOffset * (zDomain??[]).length
     marginRight = this.marginRight
     marginBottom = this.marginBottom
     yRange =  [height - marginBottom, marginTop]
     xRange =  [marginLeft, width - marginRight]

    const dataFlatted = this.dataFlatted
    const X = d3.map(dataFlatted, x);
    const Y = d3.map(dataFlatted, y);
    const Z = d3.map(dataFlatted, z);

    // Compute default domains, and unique the x- and z-domains.
    if (xDomain === undefined) xDomain = X;
    if (yDomain === undefined) yDomain = [0, d3.max(Y)];
    if (zDomain === undefined) zDomain = Z;
    xDomain = new d3.InternSet(xDomain);
    zDomain = new d3.InternSet(zDomain);

    // Omit any data not present in both the x- and z-domain.
    const I = d3.range(X.length).filter(i => xDomain.has(X[i]) && zDomain.has(Z[i]));

    // Construct scales, axes, and formats.
    const xScale = d3.scaleBand(xDomain, xRange).paddingInner(xPadding);
    const xzScale = d3.scaleBand(zDomain, [0, xScale.bandwidth()]).padding(zPadding);
    const yScale = yType(yDomain, yRange);
    const zScale = d3.scaleOrdinal(zDomain, colors);
    const xAxis = d3.axisBottom(xScale).tickSizeOuter(0);
    const yAxis = d3.axisLeft(yScale).ticks(height / 60, yFormat);

    // Compute titles.
    if (title === undefined) {
      const formatValue = yScale.tickFormat(100, yFormat);
      title = i => `${X[i]}\n${Z[i]}\n${formatValue(Y[i])}`;
    } else {
      const O = d3.map(dataFlatted, d => d);
      const T = title;
      title = i => T(O[i], i, dataFlatted);
    }

    this.g.append("g")
        .attr("transform", `translate(${marginLeft},0)`)
        .call(yAxis)
        .call(g => g.select(".domain").remove())
        .call(g => g.selectAll(".tick line").clone()
            .attr("x2", width - marginLeft - marginRight)
            .attr("stroke-opacity", 0.1))
        .call(g => g.append("text")
            .attr("x", -marginLeft)
            .attr("y", 100)
            .attr("fill", "currentColor")
            .attr("text-anchor", "start")
            .text(yLabel));

    this.g.append("g")
        .selectAll("rect")
        .data(I)
        .join("rect")
        .attr("x", i => xScale(X[i]) + xzScale(Z[i]))
        .attr("y", i => yScale(Y[i]))
        .attr("width", xzScale.bandwidth())
        .attr("height", i => yScale(0) - yScale(Y[i]))
        .attr("fill", i => zScale(Z[i]))

    if(drawLegend)
    {
      // custom legend
     let runner = 2;
      (zDomain??[]).forEach( (ele) => {
      runner+=1
      const color = zScale(ele)
      const yOffset = (labelOffset * runner)  / 2
      const xOffset = runner%2? 0 : 200
      this.g.append("circle").attr("cx",xOffset).attr("cy",yOffset).attr("r", 5).style("fill",color)
      this.g.append("text").attr("x", xOffset + 10).attr("y", yOffset +4).text(ele).style("font-size", "15px").attr("alignment-baseline","middle")
      })
    }


    if (title) this.g.append("title")
        .text(title);

    this.g.append("g")
        .attr("transform", `translate(0,${height - marginBottom})`)
        .call(xAxis);

    return this.wrapper.innerHTML;

  }

}
