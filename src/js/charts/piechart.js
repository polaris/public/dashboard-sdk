import * as d3 from "d3";
import { BaseChartWidget } from "./base";

export class PieChartWidget extends BaseChartWidget {
  constructor(title = "", description, data, options = {}) {
    if (options.transform) data = (data ?? []).map(options.transform);

    super(title, description, data, options);
    this.marginRight = 100
    this.marginBottom = 60
  }

  plot(divWidth, divHeight, el) {
    const translateX = divWidth / 2,
      translateY = divHeight / 2;

    if (!this.dataIsValid) {
      this.clearAndScaleSvg(divWidth, divHeight);
      this.drawTitle();
      this.showErrorMessage(divWidth, divHeight);
      return this.wrapper.innerHTML;
    }
    const [width, height] = this.clearAndScaleSvg(divWidth, divHeight, translateX, translateY);
    this.drawTitle();
    const radius = Math.min(width, height) / 1.5;

    const pie = d3.pie().value(function (d) {
      return d.column2;
    });

    const path = d3
      .arc()
      .outerRadius(radius - 10)
      .innerRadius(0);

    const label = d3
      .arc()
      .outerRadius(radius)
      .innerRadius(radius - 80);

    const arc = this.g.selectAll(".arc").data(pie(this.data)).enter().append("g").attr("class", "arc");

    const color = d3.scaleOrdinal().domain(this.data).range(this.colorRange);

    arc
      .append("path")
      .attr("d", path)
      .attr("fill", function (d) {
        return color(d.data.column1);
      });

    arc
      .append("text")
      .attr("transform", function (d) {
        return (
          "translate(" +
          (radius + 50) * Math.sin((d.endAngle - d.startAngle) / 2 + d.startAngle) +
          ", " +
          -1 * (radius - 10) * Math.cos((d.endAngle - d.startAngle) / 2 + d.startAngle) +
          ")"
        );
      })
      .attr("dy", ".5em")
      .text(function (d) {
        return `${d.data.column2} %`;
      });

    if (this.options.showLegend)
      this.showLegend(
        this.data.map((e) => e.column1),
        color,
        divWidth
      );

    return this.wrapper.innerHTML;
  }
}
