import * as d3 from "d3";
import { BaseChartWidget } from "./base";

export class AreaChartWidget extends BaseChartWidget {
  constructor(title = "", description, data, options = {}) {
    if (options.transform) data = (data ?? []).map(options.transform);

    super(title, description, data, options);
  }

  plot(divWidth, divHeight, el) {
    const [width, height] = this.clearAndScaleSvg(divWidth, divHeight);
    this.drawTitle();

    if (!this.dataIsValid) {
      this.showErrorMessage(divWidth, divHeight);
      return this.wrapper.innerHTML;
    }

    const x = d3
      .scaleTime()
      .domain(
        d3.extent(this.data, function (d) {
          return d.column1;
        })
      )
      .range([0, width]);

    this.appendXAxis(x, height);
    this.appendXAxisLabel(width, height);
    this.appendYAxisLabel();

    const y = d3
      .scaleLinear()
      .domain([
        0,
        d3.max(this.data, function (d) {
          return +d.column2;
        }),
      ])
      .range([height, 0]);
    this.g.append("g").call(d3.axisLeft(y));

    const color = d3.scaleOrdinal().range(this.colorRange);

    // Add the area
    this.g
      .append("path")
      .datum(this.data)
      .attr("fill", (d) => color(d.column2))
      .attr("class", "chart-area")
      .attr(
        "d",
        d3
          .area()
          .x(function (d) {
            return x(d.column1);
          })
          .y0(y(0))
          .y1(function (d) {
            return y(d.column2);
          })
      );

    if (this.options.showLegend)
      this.showLegend(
        this.data.map((e) => e.column1),
        color,
        divWidth
      );

    return this.wrapper.innerHTML;
  }
}
