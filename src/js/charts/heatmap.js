import * as d3 from "d3";
import { BaseChartWidget } from "./base";
//import { interpolateGreens } from "d3-scale-chromatic";

export class HeatMapWidget extends BaseChartWidget {
  constructor(title, description, data, options) {
    if (options.transform) data = (data ?? []).map(options.transform);

    super(title, description, data, options);
  }

  plot(divWidth, divHeight, el) {
    this.marginRight = 40
    this.marginBottom = 40
    const [width, height] = this.clearAndScaleSvg(divWidth, divHeight,20,20);
    this.drawTitle();

    if (!this.dataIsValid) {
      this.showErrorMessage(divWidth, divHeight);
      return this.wrapper.innerHTML;
    }
    const groups = d3.map(this.data, function(d){return d.group;})
    const vars = d3.map(this.data, function(d){return d.variable;})

    const xScale = d3.scaleBand().range([0, width]).padding(0.1);
    const yScale = d3.scaleBand().range([height, 0]).padding(0.1);

    xScale.domain(groups);
    yScale.domain(vars);

    if (this.options.showLegend)
    {
      this.g.append("g")
      .style("font-size", 15)
      .attr("transform", "translate(0," + height + ")")
      .call(d3.axisBottom(xScale).tickSize(0))
      .select(".domain").remove();

      this.g.append("g")
      .style("font-size", 15)
      .call(d3.axisLeft(yScale).tickSize(0))
      .select(".domain").remove()
    }

    const max = d3.max(this.data, (d) => d.value);

    const colorScale = d3.scaleSequential().interpolator(d3.interpolateGreens).domain([0,max])

    this.g.selectAll()
    .data(this.data, function(d) {return d.group+':'+d.variable;})
    .enter()
    .append("rect")
      .attr("x", function(d) { return xScale(d.group) })
      .attr("y", function(d) { return yScale(d.variable) })
      .attr("rx", 4)
      .attr("ry", 4)
      .attr("width", xScale.bandwidth() )
      .attr("height", yScale.bandwidth() )
      .style("fill", function(d) { return colorScale(d.value)} )
      .style("stroke-width", 4)
      .style("stroke", "none")
      .style("opacity", 0.8);

    return this.wrapper.innerHTML;
  }
}
