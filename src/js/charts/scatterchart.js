import * as d3 from "d3";
import { BaseChartWidget } from "./base";

// options.highlight -> {column1, column2}

export class ScatterChartWidget extends BaseChartWidget {
  constructor(title, description, data, options) {
    if (options.transform) data = (data ?? []).map(options.transform);

    super(title, description, data, options);
  }

  plot(divWidth, divHeight, el) {
    this.marginRight = 50;
    this.marginBottom = 50;
    const [width, height] = this.clearAndScaleSvg(divWidth, divHeight,20,40);
    this.drawTitle();

    if (!this.dataIsValid) {
      this.showErrorMessage(divWidth, divHeight);
      return this.wrapper.innerHTML;
    }

    const circle_color = "color" in this.options ? this.options.color : "#001122";
    const highlight_color = "highlight_color" in this.options ? this.options.highlight_color : "#00AA44";

    const xmax = "xmax" in this.options ? this.options.xmax : d3.max(this.data, (d) => d.column1);
    const ymax = "ymax" in this.options ? this.options.ymax : d3.max(this.data, (d) => d.column2);

    const xScale = d3.scaleLinear().range([0, width]);
    const yScale = d3.scaleLinear().range([0, height]);

    xScale.domain([0, xmax]);
    yScale.domain([0, ymax]);

    const showAxes = "showAxes" in this.options ? this.options.showAxes : true;
    if(showAxes) {
      this.appendXAxis(xScale, height);
      this.appendXAxisLabel(width, height);
      this.appendYAxisLabel();
      this.g.append("g").attr("transform", "translate(0, 0)").call(d3.axisLeft(yScale));
    }

    this.g.append('g')
    .selectAll("dot")
    .data(this.data)
    .enter()
    .append("circle")
      .attr("cx", function (d) { return xScale(d.column1); } )
      .attr("cy", function (d) { return yScale(d.column2); } )
      .attr("r", 2.5)
      .style("fill", function(d) { return "highlight" in d ? (d.highlight ? highlight_color : circle_color) : circle_color} )

    return this.wrapper.innerHTML;
  }
}
