import Chart from 'chart.js/auto';
import { v4 as uuidv4 } from 'uuid';

export class ChartJSWidget {
    constructor(title, description, data, options = {}, type="bar") {
    this.title = title;
    this.description = description;
    this.data = data;
    this.options = options;

    this.type = type;
    this.dataIsValid = Array.isArray(data) ? data.length > 0 : !!data;
  }


  plot(divWidth, divHeight, element) {
      element.style = "background-color: #f5f5f5; padding: 5px;";

      const title_element = document.createElement("div");
      title_element.className = "chart-title grid-title";
      title_element.innerHTML = this.title;
      title_element.style = "margin-bottom: 5px";
      if (this.description) {
          const question_mark_element = document.createElement("div");
          question_mark_element.className = "question-mark-legacy";
          question_mark_element.innerHTML = "?";
          const randomId = Date.now();
          question_mark_element.id = `id-${randomId}`;
          title_element.appendChild(question_mark_element);

          setTimeout(() => {
              const targetEl = document.getElementById(`id-${randomId}`);
              if (targetEl)
                  targetEl.addEventListener("click", (event) => {
                      this.options.onShowDesc(this.description);
                  });
          });
      }
      element.appendChild(title_element);

      const card_element = document.createElement("div");
      card_element.className = "card"
      element.appendChild(card_element);

      this.canvas = document.createElement("canvas");
      this.canvas.id = uuidv4();
      card_element.appendChild(this.canvas);
      this.canvas.style.backgroundColor = '#fff';

      this.chart = new Chart(this.canvas.id, {
        type: this.type,
        data: this.data,
        options: this.options.chartjs_options
      });

      return null;
  }

}
