import * as d3 from "d3";
import { BaseChartWidget } from "./base";

export class BarChartWidget extends BaseChartWidget {
  constructor(title, description, data, options) {
    if (options.transform) data = (data ?? []).map(options.transform);

    super(title, description, data, options);
  }

  plot(divWidth, divHeight, el) {
    const [width, height] = this.clearAndScaleSvg(divWidth, divHeight);
    this.drawTitle();

    if (!this.dataIsValid) {
      this.showErrorMessage(divWidth, divHeight);
      return this.wrapper.innerHTML;
    }

    const xScale = d3.scaleBand().range([0, width]).padding(0.4);
    const yScale = d3.scaleLinear().range([height, 0]);

    const max = d3.max(this.data, (d) => d.column2);

    xScale.domain(this.data.map((d) => d.column1));
    yScale.domain([0, max]);

    this.appendXAxis(xScale, height);
    this.appendXAxisLabel(width, height);
    this.appendYAxisLabel();

    this.g.append("g").attr("transform", "translate(0, 0)").call(d3.axisLeft(yScale));

    const color = d3.scaleOrdinal().range(this.colorRange);

    this.g
      .selectAll(".bar")
      .data(this.data)
      .enter()
      .append("rect")
      .attr("fill", (d) => color(d.column2))
      .attr("x", (d) => xScale(d.column1))
      .attr("y", (d) => yScale(d.column2))
      .attr("width", xScale.bandwidth())
      .attr("height", (d) => height - yScale(d.column2));

    if (this.options.showLegend)
      this.showLegend(
        this.data.map((e) => e.column1),
        color,
        divWidth
      );

    return this.wrapper.innerHTML;
  }
}
