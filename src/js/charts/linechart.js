import * as d3 from "d3";
import { BaseChartWidget } from "./base";

export class LineChartWidget extends BaseChartWidget {
  constructor(title = "", description, data, options = {}) {
    if (options.transform) data = (data ?? []).map(options.transform);

    super(title, description, data, options);
  }

  plot(divWidth, divHeight, el) {
    const [width, height] = this.clearAndScaleSvg(divWidth, divHeight);
    this.drawTitle();

    if (!this.dataIsValid) {
      this.showErrorMessage(divWidth, divHeight);
      return this.wrapper.innerHTML;
    }

    const [xMin, xMax] = d3.extent(this.data, (d) => d.column1);

    let xScale = null
    if(this.options.useTimeScale)
    {
      xScale = d3.scaleTime().domain([xMin, xMax]).range([0, width]);
    }
    else
    {
      xScale = d3.scaleLinear().domain([xMin, xMax]).range([0, width]);
    }

    this.appendXAxis(xScale, height);
    this.appendXAxisLabel(width, height);
    this.appendYAxisLabel();

    const yMax = d3.max(this.data, (d) => +d.column2);

    const yScale = d3.scaleLinear().domain([0, yMax]).range([height, 0]);

    this.g.append("g").call(d3.axisLeft(yScale));

    const color = d3.scaleOrdinal().range(this.colorRange);

    this.g
      .append("path")
      .datum(this.data)
      .attr("stroke", (d) => color(d.column2))
      .attr("class", "chart-path")
      .attr(
        "d",
        d3
          .line()
          .x((d) => xScale(d.column1))
          .y((d) => yScale(d.column2))
      );

    if (this.options.showLegend)
      this.showLegend(
        this.data.map((e) => e.column1),
        color,
        divWidth
      );

    return this.wrapper.innerHTML;
  }

  appendXAxis(x, height) {
    const xAxis = this.options.ticks
        ? d3.axisBottom(x).ticks(this.options.ticks)
        : d3.axisBottom(x);
    if(this.options.useTimeScale)
    {
      xAxis.tickFormat(d3.timeFormat("%d.%m.%Y %H:%M"))
    }

    this.g
        .append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(xAxis);
  }
}
