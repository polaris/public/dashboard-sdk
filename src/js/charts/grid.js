import * as d3 from "d3";
import { BaseChartWidget } from "./base";

import { AreaChartWidget } from "./areachart";
import { BarChartWidget } from "./barchart";
import { ChartJSWidget } from "./chartjs";
import { GroupedBarChartWidget } from "./grouped_barchart";
import { HeatMapWidget } from "./heatmap";
import { LineChartWidget } from "./linechart";
import { PieChartWidget } from "./piechart";
import { SimpleGroupedBarChartWidget } from "./simple_grouped_barchart";
import { StackedBarChartWidget } from "./stacked_barchart";
import {TextElement} from "../elements/textelement";
import {TextAreaElement} from "../elements/textareaelement";
import { ScatterChartWidget } from "./scatterchart";
import {BoxPlotWidget} from "./boxplot";


const chart_types = [
  "areachart",
  "barchart",
  "chartjs",
  "grouped_barchart",
  "heatmap",
  "linechart",
  "piechart",
  "simple_grouped_barchart",
  "stacked_barchart",
  "scatterchart"
]


/*
 * // config: row oder column
  [
      {type: "", name: "", options: {}, data: []},
      ...
  ]
 *
 *
 */

export class GridWidget extends BaseChartWidget {
  constructor(title, description, data, options) {
    if (options.transform) data = (data ?? []).map(options.transform);

    super(title, description, data, options);

    this.svg.remove();
    delete this.svg;
    delete this.wrapper;
  }

  plot(divWidth, divHeight, el) {
    //const [width, height] = this.clearAndScaleSvg(divWidth, divHeight);
    //this.drawTitle(); // TODO: override!

    if (!this.dataIsValid) {
      console.log("Error: Grid data invalid")
      //this.showErrorMessage(divWidth, divHeight); // TODO: makes no sense here, display error another way
      return "";// this.wrapper.innerHTML;
    }

    const title_element = document.createElement("div");
    title_element.className = "chart-title grid-title";
    title_element.innerHTML = this.title;
    if (this.description) {
      const question_mark_element = document.createElement("div");
      question_mark_element.className = "question-mark-legacy";
      question_mark_element.innerHTML = "?";
      const randomId = Date.now();
      question_mark_element.id = `id-${randomId}`;
      title_element.appendChild(question_mark_element);

      setTimeout(() => {
        const targetEl = document.getElementById(`id-${randomId}`);
        if (targetEl)
          targetEl.addEventListener("click", (event) => {
            this.options.onShowDesc(this.description);
          });
      });

    }
    el.appendChild(title_element);
    el.style = "background-color: #f5f5f5";

    let width = divWidth;
    let height = divHeight;

    const cardHolder = document.createElement("div");
    cardHolder.className = "card";
    cardHolder.style = "margin: 5px";

    const widget = document.createElement("div");
    widget.style.display = "flex";

    if (this.options.direction) {
      widget.style.flexDirection = this.options.direction; // row, column
    }
    else {
      widget.style.flexDirection = "row"; // default
    }

    let temp_subtitle = document.createElement("div");
    temp_subtitle.className = "chart-subtitle";
    temp_subtitle.innerHTML = "temp";
    el.appendChild(temp_subtitle);
    height -= title_element.offsetHeight;
    height -= temp_subtitle.offsetHeight;
    height -= 12; // margin
    el.removeChild(temp_subtitle);

    cardHolder.appendChild(widget)
    el.appendChild(cardHolder);

    if(widget.style.flexDirection == "row") {
      width -= 12; // TODO Padding / Margins korrekt einberechnen
      width /= this.data.length;
    } else {
      height -= this.data.filter((widget) => widget.name != "").length * 16;
      height /= this.data.length;
      width -= 12;
    }

    let firstColumn = true;
    for(const chart of this.data)
    {
      let subwidget_div = document.createElement("div");
      if(!firstColumn && this.options.direction !== "column")
        subwidget_div.style = "border-left: 1px solid grey";
      widget.appendChild(subwidget_div);

      let subwidget = null;
      switch(chart.type)
      {
        case "textAreaElement":
          subwidget = new TextAreaElement(
              chart.data.text,
              chart.options
          );
          break;
        case "textElement":
          subwidget = new TextElement(
              chart.data.text,
              chart.options
          );
          break;
        case "areachart":
          subwidget = new AreaChartWidget(
            "",
            "",
            chart.data,
            chart.options
          );
          break;
        case "barchart":
          subwidget = new BarChartWidget(
            "",
            "",
            chart.data,
            chart.options
          );
          break;
        case "boxplot":
          subwidget = new BoxPlotWidget(
              "",
              "",
              chart.data,
              chart.options
          );
          break;
        case "chartjs":
          subwidget = new ChartJSWidget(
            "",
            "",
            chart.data,
            chart.options
          );
          break;
        case "grid":
          subwidget = new GridWidget(
              "",
              "",
              chart.data,
              chart.options
          )
          break;
        case "grouped_barchart":
          subwidget = new GroupedBarChartWidget(
            "",
            "",
            chart.data,
            chart.options
          );
          break;
        case "heatmap":
          subwidget = new HeatMapWidget(
            "",
            "",
            chart.data,
            chart.options
          );
          break;
        case "linechart":
          subwidget = new LineChartWidget(
            "",
            "",
            chart.data,
            chart.options
          );
          break;
        case "piechart":
          subwidget = new PieChartWidget(
            "",
            "",
            chart.data,
            chart.options
          );
          break;
        case "simple_grouped_barchart":
          subwidget = new SimpleGroupedBarChartWidget(
            "",
            "",
            chart.data,
            chart.options
          );
          break;
        case "stacked_barchart":
          subwidget = new StackedBarChartWidget(
            "",
            "",
            chart.data,
            chart.options
          );
          break;
        case "scatterchart":
          subwidget = new ScatterChartWidget(
            "",
            "",
            chart.data,
            chart.options
          );
          break;
        default: // unknown -> render nothing
          widget.removeChild(subwidget_div);
          continue;
      }

      if(this.options.direction === "column") {

        subwidget_div.querySelector('.chart-title')?.parentElement.remove();
        subwidget_div.querySelector('.question-mark')?.parentElement.remove();
        // TODO paint axes yes / no ?

        const subwidget_title = document.createElement("div");
        subwidget_title.className = "chart-subtitle column";
        subwidget_title.innerHTML = chart.name;
        subwidget_div.appendChild(subwidget_title);

        const generated = subwidget.plot(width, height, subwidget_div);
        if (generated != null) {
          subwidget_div.innerHTML = subwidget_div.innerHTML + generated;
        }
      } else {

        subwidget_div.querySelector('.chart-title')?.parentElement.remove();
        subwidget_div.querySelector('.question-mark')?.parentElement.remove();
        // TODO paint axes yes / no ?

        const subwidget_title = document.createElement("div");
        subwidget_title.className = "chart-subtitle";
        subwidget_title.innerHTML = chart.name;
        subwidget_div.appendChild(subwidget_title);

        const generated = subwidget.plot(width, height, subwidget_div);
        if (generated != null) {
          subwidget_div.innerHTML = subwidget_div.innerHTML + generated;
        }

      }
      firstColumn = false;
    }
    return null; // we write directly to the DOM, thus return null
  }
}
