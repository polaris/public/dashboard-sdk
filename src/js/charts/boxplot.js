import * as d3 from "d3";
import { BaseChartWidget } from "./base";

/*
data = {
    min: 0,
    max: 100,
    q1: 12,
    median: 35,
    q3: 67,
}

 */

export class BoxPlotWidget extends BaseChartWidget {
    constructor(title, description, data, options) {
        if (options.transform) data = (data ?? []).map(options.transform);

        super(title, description, data, options);
        this.marginRight = 10
        this.marginBottom = 20
    }

    plot(divWidth, divHeight, el) {
        const [width, height] = this.clearAndScaleSvg(divWidth, divHeight, 5, 10);

        this.drawTitle();

        /*if (!this.dataIsValid) {
            this.showErrorMessage(divWidth, divHeight);
            return this.wrapper.innerHTML;
        }*/ // TODO


        const xScale = d3.scaleLinear()
            .domain([this.data.min,this.data.max])
            .range([width, 0]);

        let center = height / 2.0;

        // Show the main horizontal line
        this.g
            .append("line")
            .attr("x1", xScale(this.data.min))
            .attr("x2", xScale(this.data.max))
            .attr("y1", center )
            .attr("y2", center )
            .attr("stroke", "black")

        // Show the box
        this.g
            .append("rect")
            .attr("x", xScale(this.data.q3))
            .attr("y", center - height / 2)
            .attr("height", height)
            .attr("width", xScale(this.data.q1) - xScale(this.data.q3))
            .attr("stroke", "black")
            .style("fill", "#69b3a2")

        // show median, min and max horizontal lines
        this.g
            .selectAll("toto")
            .data([this.data.min, this.data.median, this.data.max])
            .enter()
            .append("line")
            .attr("x1", function(d){ return(xScale(d))})
            .attr("x2", function(d){ return(xScale(d))})
            .attr("y1", center - height / 2 )
            .attr("y2", center + height / 2 )
            .attr("stroke", "black")

        return this.wrapper.innerHTML;
    }
}
