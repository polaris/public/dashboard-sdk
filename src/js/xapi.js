const IFI = require('./xapi/IFI');
const polaris = require('./xapi/polaris');

const xApi = {
    IFI,
    polaris,
};
module.exports = xApi;