const actor = require('./actor');
const answered = require('./verb/answered');
const completed = require('./verb/completed');
const failed = require('./verb/failed');
const initialized = require('./verb/initialized');
const passed = require('./verb/passed');
const registered = require('./verb/registered');
const terminated = require('./verb/terminated');
const resized = require('./verb/resized');

const polaris = {
    actor,
    answered,
    completed,
    failed,
    initialized,
    passed,
    registered,
    terminated,
    resized
};

module.exports = polaris;
