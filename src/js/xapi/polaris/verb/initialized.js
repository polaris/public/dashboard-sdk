const statement = require('../../statement');
const {cmi5} = require('../../profiles');

const initialized = () => {
    let state = {
        ...cmi5.initialized,
    };

    return {
        ...statement(state),
    };
};

module.exports = initialized;
