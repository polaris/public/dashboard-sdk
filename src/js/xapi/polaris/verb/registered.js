const statement = require('../../statement');
const {cmi5} = require('../../profiles');

const registered = () => {
    let state = {
        ...cmi5.registered,
    };

    return {
        ...statement(state),
    };
};

module.exports = registered;