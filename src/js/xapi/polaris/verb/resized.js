const statement = require('../../statement');
const { polaris} = require('../../profiles');

const resized = () => {
    let state = {};
    const baseStatement = statement(polaris.resized);

    return {
        ...baseStatement,
        getStatement() {
            return {
                ...baseStatement.getStatement(),
                ...state,
            }
        }
    };
};

module.exports = resized;