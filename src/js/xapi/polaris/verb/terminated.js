const statement = require('../../statement');
const {cmi5} = require('../../profiles');

const terminated = () => {
    let state = {
        ...cmi5.terminated,
    };

    return {
        ...statement(state),
    };
};

module.exports = terminated;
