import {BaseElement} from "./base_element";

export class TextElement extends BaseElement {

    constructor(text, options = {}) {
        super(options);
        this.text = text;
    }

    plot(divWidth, divHeight, element) {
        const title_element = document.createElement("div");
        title_element.innerHTML = this.text;
        element.appendChild(title_element);
    }
}