import {BaseElement} from "./base_element";

export class TextAreaElement extends BaseElement {

    constructor(text, options = {}) {
        super(options);
        this.text = text;
    }

    plot(divWidth, divHeight, element) {
        const title_element = document.createElement("textarea");
        title_element.placeholder = this.text;
        title_element.rows = divHeight / 24;
        title_element.height = divHeight;
        title_element.className = "form-control";
        element.appendChild(title_element);
    }
}