
import 'bootstrap/scss/_functions.scss'
import './assets/main.scss'
import 'bootstrap/scss/bootstrap.scss'

import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import VueWriter from "vue-writer";
const app = createApp(App)

app.use(router)
app.use(VueWriter)

app.mount('#app')
