# Polaris Dashboard SDK

## Overview

![](./documentation/polaris_dashboard_sdk.drawio.png)


## Getting Started

### 1. Request JWT Token from Backend

### 2. Setup HTML container

```html
<div class="wrapper">
    <div class="sidebar-hidden" id="sidebar">
    <div class="dropzone-remove">
        <div>Drop here to remove</div>
    </div>
    <div class="available-widgets" id="available-widgets"></div>
    </div>
    <div class="grid">
    <div class="grid-stack"></div>
    </div>
</div>
```

### 3. Embed dashboard scripts

```html
<script defer src="./bundle.js"></script>
```

### 4. Embed Google Font Roboto (Optional)

```html
<link
    href="https://fonts.googleapis.com/css2?family=Roboto:wght@100&display=swap"
    rel="stylesheet"
/>
```

### 5. Setup widgets position and sizes

```js
const items = [
  {
    x: 4,
    y: 0,
    w: 4,
    h: 8,
    widgetId: "second-widget",
  }]
```

### 6. Request analytics engine results with JWT Token and render widgets

```js
getResult(token).then((data) => {
  const widgets = {
    "second-widget": new BarChartWidget(
      "Statements H5P",
      data["count_h5p_statements"]?.latest_result,
      {
        xAxisLabel: "Datum",
        yAxisLabel: "#Statements",
        transform: (d) => ({
          column1: new Date(d.column1 * 1000),
          column2: d.column2,
        }),
      }
    )}

  const grid = initGrid(widgets, items);

  const toggleBtn = document.getElementById("toggle-sidebar-btn");
  toggleBtn.onclick = grid.toggleSidebar;

  const saveBtn = document.getElementById("save-btn");
  saveBtn.onclick = () => {
    const oldGrid = grid.save();

    grid.grid.removeAll();
    setTimeout(() => {
      grid.load(oldGrid);
    }, 2000);
  };
)
```