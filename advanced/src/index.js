import React from 'react';
import { createRoot } from 'react-dom/client';
import {
    createBrowserRouter,
    RouterProvider,
  } from "react-router-dom";
import './index.scss';
import Layout from './app/layout/layout';
import ErrorPage from './app/layout/errorpage';
import Dashboard from './app/pages/dashboard';
import Login from './app/pages/login';
import Dashboard2 from './app/pages/dashboard_2';
import Welcome from './app/pages/welcome';
import Dashboard3 from './app/pages/dashboard_3';
import ManualData from './app/pages/manualdata';
import XAPIStatementViewer from './app/pages/xapistatementviewer';
import Dashboard4 from './app/pages/dashboard_4';

// Clear the existing HTML content
document.body.innerHTML = '<div id="app"></div>';


const router = createBrowserRouter([
    {
        path: "/",
        element: <Layout />,
        errorElement: <ErrorPage />,
        children: [
            {
                path: "/",
                element: <Welcome />,
            },
            {
              path: "manualData",
              element: <ManualData />,
            },
            {
              path: "xapiviewer",
              element: <XAPIStatementViewer />,
            },
            {
              path: "dashboard/1",
              element: <Dashboard />,
            },
            {
                path: "dashboard/2",
                element: <Dashboard2 />,
              },
              {
                path: "dashboard/3",
                element: <Dashboard3 />,
              },
              {
                path: "dashboard/4",
                element: <Dashboard4 />,
              },
          ],
    },
    {
        path: "/login",
        element: <Login />,
        errorElement: <ErrorPage />
    },
  ]);

// Render your React component instead
const root = createRoot(document.getElementById('app'));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
