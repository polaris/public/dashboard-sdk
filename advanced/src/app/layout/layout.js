import React from 'react';
import { Container, Nav, NavDropdown, Navbar } from 'react-bootstrap';
import { Outlet } from 'react-router-dom';

export default function Layout() {
    return (
      <>
       <Navbar expand="lg" bg="primary" data-bs-theme="dark">
      <Container fluid>
        <Navbar.Brand href="/">POLARIS Dashboard</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/manualData">Datenerfassung</Nav.Link>
            <NavDropdown title="Dashboards" id="basic-nav-dropdown">
              <NavDropdown.Item href="/dashboard/1">Dashboard Base SDK</NavDropdown.Item>
              <NavDropdown.Item href="/dashboard/2">Dashboard Advanced SDK</NavDropdown.Item>
              <NavDropdown.Item href="/dashboard/3">Dashboard Advanced SDK mit Rights Engine</NavDropdown.Item>
              <NavDropdown.Item href="/dashboard/4">Dashboard Advanced SDK mit Formularen</NavDropdown.Item>
            </NavDropdown>

            <NavDropdown title="Entwickler Tools" id="basic-nav-dropdown">
              <NavDropdown.Item href="/xapiviewer">xAPI Statements Viewer</NavDropdown.Item>
            </NavDropdown>
          </Nav>
          <Nav>
          <NavDropdown title="Angemeldet als user@polaris.com" id="user-account">
              {/* <NavDropdown.Item href="/about">Über diese Seite</NavDropdown.Item> */}
              <NavDropdown.Item href="/login">Abmelden</NavDropdown.Item>
            </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>

    <Outlet />
      </>
    );
  }