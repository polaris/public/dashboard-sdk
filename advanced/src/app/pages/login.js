import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";

import CoverImage from "./cover.jpg"
import { faKey, faUser } from "@fortawesome/free-solid-svg-icons";

export default function Login() {


    const [username, setUsername] = useState("")
    const [password, setPassword] = useState("")
    const [isLoading, setIsLoading] = useState(false)

    const login =  () => {
        window.location.href = "/"
    }

    const onKeyDown = (event) => {
        if (event.key === "Enter") {
            event.preventDefault();
            login()
        }
    }




    return    <Container fluid><Row style={{minHeight: "100vh"}}>
    <Col md={4} lg={6} xl={8} className={"d-none d-md-block video-container"} style={{backgroundImage: "url("+ CoverImage + ")", backgroundSize: "cover"}}>
    </Col>
    <Col md={8} lg={6} xl={4}>
        <div className="form-left px-5">
            <div  className="row g-4"  style={{marginTop: "50px"}}>
                  <h3>POLARIS</h3>
            </div>
            <div  className="row g-4 mt-2"  style={{borderLeft: "4px solid #3f51b5"}}>
                <h4 className={""}>Bitte melden Sie sich an.</h4>
                <div className="col-12">
                    <label>Nutzername<span className="text-danger">*</span></label>
                    <div className="input-group">
                        <div className="input-group-text"><FontAwesomeIcon icon={faUser} /></div>
                        <input onKeyDown={onKeyDown} onChange={(e) => setUsername(e.target.value)} value={username} type="text" className="form-control" placeholder="E-Mail Adresse"/>
                    </div>
                </div>

                <div className="col-12">
                    <label>Passwort<span className="text-danger">*</span></label>
                    <div className="input-group">
                        <div className="input-group-text"><FontAwesomeIcon  icon={faKey}/></div>
                        <input onKeyDown={onKeyDown} onChange={(e) => setPassword(e.target.value)} value={password} type="password" className="form-control" placeholder="Bitte geben Sie Ihr Passwort ein"/>
                    </div>
                </div>

                <div className="col-sm-12">
                    <Button onClick={() => login()} className="btn btn-primary px-4 float-end"><FontAwesomeIcon icon="sign-in"/> Anmelden </Button>
                </div>
            </div>
            <div className={"text-center text-muted"}>
                Impressum • Datenschutz
            </div>
        </div>
    </Col>
</Row></Container>
}