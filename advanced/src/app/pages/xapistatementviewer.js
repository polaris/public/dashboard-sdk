import React from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import { ReactTabulator, reactFormatter } from "react-tabulator";

export default function XAPIStatementViewer() {

    const data = [
        {
          id: 1,
          name: "Oli Bob",
          age: "12",
          color: "red",
          dob: "01/01/1980",
          rating: 5,
          passed: true,
          pets: ["cat", "dog"]
        },
        {
          id: 2,
          name: "Mary May",
          age: "1",
          color: "green",
          dob: "12/05/1989",
          rating: 4,
          passed: true,
          pets: ["cat"]
        },
      ];

      const options = {
        movableColumns: true
      };

      const columns = [
        { title: "Actor", field: "name"  },
        { title: "Verb", field: "age" },
        { title: "Objekt", field: "object" },
        { title: "Context", field: "context" },
        { title: "Grouping", field: "grouping" },
        { title: "Result", field: "result" },
      ];

    return <Container className="mt-5">
    <h3>xAPI Statements</h3>
        <Card>
        <ReactTabulator
          columns={columns}
          data={data}
          options={options}
        />
        </Card>
    </Container>
}