import React, { useEffect } from 'react';
import {mockBarChart, mockChartJSBarChart} from "../../mock_charts/mock_bar_chart";
import {mockGrid} from "../../mock_charts/mock_grid";
import {mockScatterGrid} from "../../mock_charts/mock_scatter_grid";
import {mockPieGrid} from "../../mock_charts/mock_pie_grid";
import {mockDeadlines} from "../../mock_charts/mock_deadlines";
import {mockProgressHeat} from "../../mock_charts/mock_progress_heat";
import {mockProgressBox} from "../../mock_charts/mock_progress_box";
import {mockFeedback} from "../../mock_charts/mock_feedback";
import GridLayout from "react-grid-layout";
import PolarisDashboard from '../components/PolarisDashboard';
import PolarisDashboardSplitView from '../components/PolarisDashboardSplitView';


export default function Dashboard3() {

    const dashboardConfig = {
        "id": "new-unique-id",
        "name": "Simple Dashboard",
        "visualisationToken": "",
        "rights_engine": "https://polaris.digitallearning.gmbh",
        "grid": {
            "cols": 6
        },
        "interaction": {
            "allowEdit": true,
            "allowRemove": true,
            "allowAdd": true,
            "autoSave": true,
        },
        "widgets": {
            "welcome-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine einfache Karte",
                    "body": "Hier könnte ein erklärender Text stehen"
                }
            },
            "info-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine zweite Karte",
                    "body": "Diese Karte kann man bewerten und man kann HTML verwenden <b>HTML</b>",
                    "variant": "primary",
                },
                "feedback": {
                    "mode": "thumbs",
                    "color": "light",
                }
            }
        }
    };

    const dashboardConfig2 = {
        "id": "new-unique-id",
        "name": "Simple Dashboard",
        "visualisationToken": "",
        "rights_engine": "https://polaris.digitallearning.gmbh",
        "grid": {
            "cols": 6
        },
        "interaction": {
            "allowEdit": true,
            "allowRemove": true,
            "allowAdd": true,
            "autoSave": true,
            "feedback": {
                "addWidget": true,
                "removeWidget": true,
                "reorderWidgets": true, 
            }
        },
        "widgets": {
            "welcome-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine einfache Karte",
                    "body": "Hier könnte ein erklärender Text stehen"
                }
            },
            "primary-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine zweite Karte",
                    "body": "Diese Karte kann HTML verwenden <b>HTML</b>",
                    "variant": "primary",
                }
            },
            "secondary-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine zweite Karte",
                    "body": "Diese Karte kann HTML verwenden <b>HTML</b>",
                    "variant": "secondary",
                }
            },
            "warning-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine zweite Karte",
                    "body": "Diese Karte kann HTML verwenden <b>HTML</b>",
                    "variant": "warning",
                },
                "layout": {
                    "w": 4,
                    "h": 4
                }
            },
            "alert-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine zweite Karte",
                    "body": "Diese Karte kann HTML verwenden <b>HTML</b>",
                    "variant": "danger",
                }
            },
            "info-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine zweite Karte",
                    "body": "Diese Karte kann HTML verwenden <b>HTML</b>",
                    "variant": "info",
                },
                "layout": {
                    "w": 4
                }
            }
        }
    };


    return <>
    <PolarisDashboardSplitView config={dashboardConfig} title={"Base example with auto save"}/>
    <PolarisDashboardSplitView config={dashboardConfig2} title={"Base example dashboard as data provider"}/>
    </>
    
}