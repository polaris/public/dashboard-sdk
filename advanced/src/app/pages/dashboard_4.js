import React, { useEffect } from 'react';
import {mockBarChart, mockChartJSBarChart} from "../../mock_charts/mock_bar_chart";
import {mockGrid} from "../../mock_charts/mock_grid";
import {mockScatterGrid} from "../../mock_charts/mock_scatter_grid";
import {mockPieGrid} from "../../mock_charts/mock_pie_grid";
import {mockDeadlines} from "../../mock_charts/mock_deadlines";
import {mockProgressHeat} from "../../mock_charts/mock_progress_heat";
import {mockProgressBox} from "../../mock_charts/mock_progress_box";
import {mockFeedback} from "../../mock_charts/mock_feedback";
import GridLayout from "react-grid-layout";
import PolarisDashboard from '../components/PolarisDashboard';
import PolarisDashboardSplitView from '../components/PolarisDashboardSplitView';


export default function Dashboard4() {

    const dashboardConfig = {
        "id": "new-unique-id",
        "name": "Simple Dashboard",
        "visualisationToken": "",
        "rights_engine": "https://polaris.digitallearning.gmbh",
        "grid": {
            "cols": 6
        },
        "interaction": {
            "allowEdit": true,
            "allowRemove": true,
            "allowAdd": true,
            "autoSave": true,
        },
        "widgets": {
            "welcome-card": {
                "type": "form",
                "parameters": {
                    "headline": "Das ist ein einfaches Formular",
                    "body": "Hier könnte ein erklärender Text stehen",
                    "form": [
                        {
                            "type": "textBox",
                            "label": "Einfaches Textfeld",
                        },
                        {
                            "type": "textArea",
                            "label": "TextArea"
                        }
                    ]
                },
                "layout": {
                    "w": 6,
                    "h": 3
                }
            },            
        }
    };


    return <>
    <PolarisDashboardSplitView config={dashboardConfig} title={"Base example with forms"}/>
    </>
    
}