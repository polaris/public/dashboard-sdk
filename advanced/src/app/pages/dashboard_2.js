import React, { useEffect } from 'react';
import {mockBarChart, mockChartJSBarChart} from "../../mock_charts/mock_bar_chart";
import {mockGrid} from "../../mock_charts/mock_grid";
import {mockScatterGrid} from "../../mock_charts/mock_scatter_grid";
import {mockPieGrid} from "../../mock_charts/mock_pie_grid";
import {mockDeadlines} from "../../mock_charts/mock_deadlines";
import {mockProgressHeat} from "../../mock_charts/mock_progress_heat";
import {mockProgressBox} from "../../mock_charts/mock_progress_box";
import {mockFeedback} from "../../mock_charts/mock_feedback";
import GridLayout from "react-grid-layout";
import PolarisDashboard from '../components/PolarisDashboard';
import PolarisDashboardSplitView from '../components/PolarisDashboardSplitView';


export default function Dashboard2() {

    const dashboardConfig = {
        "id": "new-unique-id",
        "name": "Simple Dashboard",
        "grid": {
            "cols": 6
        },
        "widgets": {
            "welcome-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine einfache Karte",
                    "body": "Hier könnte ein erklärender Text stehen"
                }
            }
        }
    };

    const dashboardConfig2 = {
        "id": "new-unique-id",
        "name": "Simple Dashboard",
        "widgets": {
            "welcome-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine einfache Karte",
                    "body": "Hier könnte ein erklärender Text stehen"
                }
            },
            "info-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine zweite Karte",
                    "body": "Diese Karte kann man bewerten und man kann HTML verwenden <b>HTML</b>",
                    "variant": "primary",
                },
                "feedback": {
                    "mode": "thumbs",
                    "color": "light",
                }
            }
        }
    };

    const dashboardConfig3 = {
        "id": "new-unique-id",
        "name": "Simple Dashboard",
        "interaction": {
            "allowEdit": true,
            "allowRemove": true,
            "allowAdd": true,
        },
        "widgets": {
            "welcome-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine einfache Karte",
                    "body": "Hier könnte ein erklärender Text stehen"
                }
            },
            "info-card": {
                "type": "card",
                "parameters": {
                    "headline": "Das ist eine zweite Karte",
                    "body": "Diese Karte kann man bewerten und man kann HTML verwenden <b>HTML</b>",
                    "variant": "primary",
                },
                "feedback": {
                    "mode": "thumbs",
                    "color": "light",
                }
            }
        }
    };

    const dashboardConfig4 = {
        "id": "new-unique-id",
        "name": "Simple Dashboard",
        "widgets": {
            "no-rating-card": {
                "type": "card",
                "parameters": {
                    "headline": "Eine Karte ohne Feedback",
                    "body": "Diese Karte kann nicht bewertet werden"
                }
            },
            "thumbs-card": {
                "type": "card",
                "parameters": {
                    "headline": "Daumen hoch/runter",
                    "body": "Diese Karte kann mit <b>Daumen hoch</b> oder <i>Daumen runter</i> bewertet werden",
                    "variant": "primary",
                },
                "feedback": {
                    "mode": "thumbs",
                    "color": "light",
                },
                "layout": {
                    "w": 4
                }
            },
            "start-card": {
                "type": "card",
                "parameters": {
                    "headline": "Diese Karte kann per Sterne bewertet werden",
                    "body": "Diese Karte kann man mit Sternen bewerten",
                    "variant": "warning",
                },
                "feedback": {
                    "mode": "star",
                    "color": "light",
                }
            },
            "text-card": {
                "type": "card",
                "parameters": {
                    "headline": "Diese Karte kann per Freitext bewertet werden",
                    "body": "Diese Karte kann man mit einem Freitext bewerten",
                    "variant": "secondary",
                },
                "feedback": {
                    "mode": "text",
                    "color": "light",
                }
            }
        }
    };

    const dashboardConfig5 = {
        "id": "new-unique-id",
        "name": "Simple Dashboard",
        "interaction": {
            "allowEdit": true,
            "allowRemove": true,
            "allowAdd": true,
        },
        "widgets": {
            "no-rating-card": {
                "type": "card",
                "parameters": {
                    "headline": "Eine Karte ohne Feedback",
                    "body": "Diese Karte kann nicht bewertet werden"
                }
            },
            "linechart": {
                "type": "lineChart",
                "parameters": {
                    "headline": "Kursnutzung",
                    "description": "Durchschnittliche Kursnutzung nach Blöcken",
                    "data": [ {column1: 0, column2: 80},
                        {column1: 10, column2: 60},
                        {column1: 20, column2: 70},
                        {column1: 30, column2: 20},
                        {column1: 40, column2: 0},
                        {column1: 50, column2: 60},
                        {column1: 60, column2: 70},
                        {column1: 70, column2: 80},
                        {column1: 80, column2: 76},
                        {column1: 90, column2: 60},
                        {column1: 100, column2: 70}],
                    "yAxisLabel": "Teilnahme",
                    "xAxisLabel": "Blöcke"
                },
                "layout": {
                    "w": 8
                }
            },
        }
    };

    const dashboardConfig6 = {
        "id": "new-unique-id",
        "name": "Simple Dashboard",
        "interaction": {
            "allowEdit": true,
            "allowRemove": true,
            "allowAdd": true,
        },
        "widgets": {
            "no-rating-card": {
                "type": "card",
                "parameters": {
                    "headline": "Eine Karte ohne Feedback",
                    "body": "Diese Karte kann nicht bewertet werden"
                }
            },
            "linechart": {
                "type": "lineChart",
                "parameters": {
                    "headline": "Kursnutzung",
                    "description": "Durchschnittliche Kursnutzung nach Blöcken",
                    "data": [ {column1: 0, column2: 80},
                        {column1: 10, column2: 60},
                        {column1: 20, column2: 70},
                        {column1: 30, column2: 20},
                        {column1: 40, column2: 0},
                        {column1: 50, column2: 60},
                        {column1: 60, column2: 70},
                        {column1: 70, column2: 80},
                        {column1: 80, column2: 76},
                        {column1: 90, column2: 60},
                        {column1: 100, column2: 70}],
                    "yAxisLabel": "Teilnahme",
                    "xAxisLabel": "Blöcke"
                },
                "layout": {
                    "w": 8
                }
            },
            "barchart": {
                "type": "barChart",
                "parameters": {
                    "headline": "A sample bar chart",
                    "description": "Beschreibung",
                    "data": {
                        "accessed": 98,
                        "started": 7,
                        "graded": 7,
                        "submitted": 7,
                        "answered": 38,
                        "leftUnanswered": 4
                    },
                    "yAxisLabel": "Status",
                    "xAxisLabel": "Anzahl"
                },
                "layout": {
                    "w": 4
                }
            },
            "guagechart": {
                "type": "guageChart",
                "parameters": {
                    "headline": "A sample guage chart",
                    "description": "Beschreibung",
                    "data" : {
                        "nrOfLevels": 20,
                        "percent": 0.80
                    }
                },
                "layout": {
                    "w": 4,
                    "h": 2
                }
            },
        }
    };


    return <>
    <PolarisDashboardSplitView config={dashboardConfig} title={"Base example"}/>
    <PolarisDashboardSplitView config={dashboardConfig2} title={"Base example with two Widgets"}/>
    <PolarisDashboardSplitView config={dashboardConfig3} title={"Base example with edit mode"}/>
    <PolarisDashboardSplitView config={dashboardConfig4} title={"Rating examples"}/>
    <PolarisDashboardSplitView config={dashboardConfig5} title={"Chart examples"}/>
    <PolarisDashboardSplitView config={dashboardConfig6} title={"Complex Chart examples"} hideConfig={true} height={"800px"}/>
    </>
    
}