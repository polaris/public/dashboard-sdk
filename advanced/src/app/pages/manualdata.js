import React from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import SyntaxHighlighter from "react-syntax-highlighter/dist/esm/default-highlight";
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';

export default function ManualData() {

    return <Container className="mt-5">
        <Row>
            <Col>
             <Card>
                <Card.Body>
                    <h5>Aktivität protokollieren</h5>
                </Card.Body>
             </Card>
            </Col>
            <Col>
                <SyntaxHighlighter language="javascript" style={docco}>
                </SyntaxHighlighter>
            </Col>
        </Row>
    </Container>
}