import React from "react";
import { Card, Col, Container, Row } from "react-bootstrap";


import CoverImage from "./cover.jpg"

export default function Welcome() {

    return   <Container fluid>
        <Row style={{minHeight: "100vh"}}>
    <Col lg={2} xl={4} className={"d-none d-lg-block video-container"} style={{backgroundImage: "url("+ CoverImage + ")", backgroundSize: "cover"}}>
    </Col>
    <Col md={12} lg={10} xl={8}>
        <Card className="m-5">
            <Card.Body>
                <h1>POLARIS – Provider Oriented Open Learning Analytics</h1> 
                <p className="text-muted">entwickelt im Rahmen des KI.edu.NRW Projekts mit der Ruhr-Universtität-Bochum und der RWTH Aachen</p>
                
                <h3>Was ist POLARIS?</h3>
            
                <h3>Wie nutze ich POLARIS?</h3>
            
                <h3>Kann ich POLARIS selber betreiben?</h3>

                <h3>Wo finde ich den Quellcode von POLARIS?</h3>
            </Card.Body>
        </Card>
    </Col>
    </Row>
    </Container>
} 