import React, { useState } from "react";
import { Button, Form, FormControl, Modal } from "react-bootstrap";
import ReactStars from 'react-stars'

export default function PolarisTextRatingModal({show, onHide, configuration})
{
    let [text, setText] = useState(undefined);


    return <Modal show={show} onHide={onHide}>
    <Modal.Header closeButton>
      <Modal.Title>
        {configuration?.title ?? "Wie finden Sie dieses Element?"}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
        {configuration?.descriptionText}
        <Form.Control onChange={(evt) => setText(evt.target.value)} as="textarea" rows={configuration?.rows ?? 3} placeholder={configuration?.placeholder ?? "Bitte bewerten Sie das Element"} />
        </Modal.Body>
    <Modal.Footer>
      <Button variant="secondary" onClick={onHide}>
        Abbrechen
      </Button>
      <Button variant="primary" onClick={onHide}>
        {configuration?.submitButton ?? "Feedback absenden"}
      </Button>
    </Modal.Footer>
  </Modal>
}