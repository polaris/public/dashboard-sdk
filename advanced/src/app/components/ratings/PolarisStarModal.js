import React, { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import ReactStars from 'react-stars'

export default function PolarisStarModal({show, onHide, configuration})
{
    let [rating, setRating] = useState(3);


    return <Modal show={show} onHide={onHide}>
    <Modal.Header closeButton>
      <Modal.Title>
        {configuration?.title ?? "Wie finden Sie dieses Element?"}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
        {configuration?.descriptionText}
        <div className="ms-auto me-auto">
        <ReactStars
  count={configuration?.maxStars ?? 5}
  value={rating}
  onChange={(evt) => setRating(evt)}
  size={50}
  color2={'#ffd700'} /></div>
        </Modal.Body>
    <Modal.Footer>
      <Button variant="secondary" onClick={onHide}>
        Abbrechen
      </Button>
      <Button variant="primary" onClick={onHide}>
        {configuration?.submitButton ?? "Feedback absenden"}
      </Button>
    </Modal.Footer>
  </Modal>
}