import React, { useEffect, useState } from "react";
import GridLayout, { WidthProvider } from "react-grid-layout";
import PolarisBaseWidget from "./widgets/PolarisBaseWidget";
import { Button, Offcanvas } from "react-bootstrap";
import { faPenToSquare, faPlus, faXmark } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";


const ResponsiveGridLayout = WidthProvider(GridLayout);

export default function PolarisDashboard({config}) {
    
    const id = config.id;
    const debug = config.debug;
    let [generatedWidgets, setGeneratedWidgets] = useState([])
    let [layout, setLayout] = useState([])
    let [isResizable, setIsResizable] = useState(false)
    let [isDraggable, setIsDraggable] = useState(false)
    let [showAddDrawer, setShowAddDrawer] = useState(false)


    useEffect(() => {
        if(config == null)
        {

        }
        forceRender()
    },[config, isDraggable,isResizable])


  const handleClose = () => setShowAddDrawer(false);

    const forceRender = () => {
        let widgets = [];
        Object.keys(config?.widgets).forEach((key, _) => { widgets.push(<div key={key}><PolarisBaseWidget isDraggable={isDraggable} isResizable={isResizable} config={config?.widgets[key]} widgetKey={key}/></div>);})
        setGeneratedWidgets(widgets)

        let tempLayout = [];
        Object.keys(config?.widgets).forEach((key, _) => { tempLayout.push({x: 0, y: 0, w: 2, h: 2, ...config?.widgets[key]?.layout, i: key})})
        if(debug)
        {
          console.log("[DEBUG] layout calculation: " + tempLayout)
        }
        setLayout(tempLayout);
    }
    
        return <>
        <div className="d-flex justify-content-between">
        <h5>{config.name}</h5>
        <div className="d-flex">
            {config.interaction?.allowEdit ? <Button variant="primary" className="m-1" onClick={() => {
                setIsDraggable(!isDraggable)
                setIsResizable(!isResizable)
            }}>{ isDraggable || isResizable ? <FontAwesomeIcon icon={faXmark} /> : <FontAwesomeIcon icon={faPenToSquare} />}</Button> : null }
            {config.interaction?.allowAdd ? <Button variant="outline-secondary" className="m-1" onClick={() => {
                setShowAddDrawer(true)
            }}><FontAwesomeIcon icon={faPlus} /></Button> : null }
        </div>
        </div>

        {generatedWidgets?.length > 0 ?
        <ResponsiveGridLayout
        className="layout"
        layout={layout}
        isDraggable={isDraggable}
        isResizable={isResizable}
        compactType={"horizontal"}
        autoSize={config?.grid?.autoSize ?? false}
      >
         {generatedWidgets?.map((widget) => {
                return widget;
            })}
      </ResponsiveGridLayout> : null }

      <Offcanvas show={showAddDrawer} onHide={handleClose}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Widgets hinzufügen</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          Klicken Sie ein Widget an, um es dem Dashboard hinzuzufügen. Danach können Sie es verschieben und anpassen.
        </Offcanvas.Body>
      </Offcanvas>
      </>

}
