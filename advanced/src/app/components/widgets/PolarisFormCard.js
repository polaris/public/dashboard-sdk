import React from "react";
import { Card, Form } from "react-bootstrap";

export default function PolarisFormCard({parameters}) 
{
    return <Card style={{ height: "100%"}} bg={parameters.variant} 
    text={parameters.variant == null || parameters.variant.toLowerCase() === 'light' ? 'dark' : 'white'}>
        <Card.Body>
         <Card.Title>
        {parameters.headline}</Card.Title>
        <Card.Text dangerouslySetInnerHTML={{ __html: parameters.body}}></Card.Text>
        <Form>
      <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
        <Form.Label>Einfaches Textfeld</Form.Label>
        <Form.Control type="text" placeholder="" />
      </Form.Group>
      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
        <Form.Label>Textarea</Form.Label>
        <Form.Control as="textarea" rows={3} />
      </Form.Group>
    </Form>
        </Card.Body>
    </Card>
}