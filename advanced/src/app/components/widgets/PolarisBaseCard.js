import React from "react";
import { Card } from "react-bootstrap";

export default function PolarisBaseCard({parameters}) 
{
    return <Card style={{ height: "100%"}} bg={parameters.variant} 
    text={parameters.variant == null || parameters.variant.toLowerCase() === 'light' ? 'dark' : 'white'}>
        <Card.Body>
         <Card.Title>
        {parameters.headline}</Card.Title>
        <Card.Text dangerouslySetInnerHTML={{ __html: parameters.body}}></Card.Text>
        </Card.Body>
    </Card>
}