import React from "react";
import { Card } from "react-bootstrap";
import GaugeChart from 'react-gauge-chart'

export default function PolarisGuageChart({parameters, isEdit})
{

    return <Card style={{height: "100%"}}  bg={parameters.variant} 
    text={parameters.variant == null || parameters.variant.toLowerCase() === 'light' ? 'dark' : 'white'}>
        <Card.Body>
        <Card.Title>
        {parameters.headline}</Card.Title>
<GaugeChart id="gauge-chart2" 
  nrOfLevels={parameters?.data?.nrOfLevels} 
  percent={parameters?.data?.percent} 
/>
        </Card.Body>
          
        </Card>
}