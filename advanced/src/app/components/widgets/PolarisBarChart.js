import React, { useEffect, useRef, useState } from "react";
import { BarChartWidget } from "@polaris/dashboard-sdk";
import { Card } from "react-bootstrap";

export default function PolarisBarChart({parameters, isEdit})
{

    const ref = useRef(null)

    const [plotData, setPlotData] = useState("")

    useEffect(() => {
        if(isEdit)
            setPlotData(lineChartLib.plot(ref.current.clientWidth,ref.current.clientHeight))
      })
  
    useEffect(() => {
      setPlotData(lineChartLib.plot(ref.current.clientWidth,ref.current.clientHeight))
    },[ref.current?.clientWidth, ref.current?.clientHeight])


    const data = Object.keys(parameters?.data).map(key => ({column1: key, column2: parameters?.data[key]}));

    console.log(data)

    const lineChartLib = new BarChartWidget(
        parameters?.headline,
        parameters?.description,
        data,
        {
            showLegend: false,
            xAxisLabel: parameters?.xAxisLabel,
            yAxisLabel:  parameters?.yAxisLabel,
            onShowDesc: () => {

            },
            disableBackground: true,
            disableQuestionMark: true,
        }
    );

    return <Card style={{height: "100%"}} ref={ref} dangerouslySetInnerHTML={{__html: plotData }}/>
}