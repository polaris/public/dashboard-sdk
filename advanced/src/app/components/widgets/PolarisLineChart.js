import React, { useEffect, useRef, useState } from "react";
import { LineChartWidget } from "@polaris/dashboard-sdk";
import { Card } from "react-bootstrap";

export default function PolarisLineChart({parameters, isEdit})
{

    const ref = useRef(null)

    const [plotData, setPlotData] = useState("")

    useEffect(() => {
        if(isEdit)
            setPlotData(lineChartLib.plot(ref.current.clientWidth,ref.current.clientHeight))
      })
  
    useEffect(() => {
      setPlotData(lineChartLib.plot(ref.current.clientWidth,ref.current.clientHeight))
    },[ref.current?.clientWidth, ref.current?.clientHeight])

    const lineChartLib = new LineChartWidget(
        parameters?.headline,
        parameters?.description,
        parameters?.data,
        {
            showLegend: false,
            xAxisLabel: parameters?.xAxisLabel,
            yAxisLabel:  parameters?.yAxisLabel,
            onShowDesc: () => {

            },
            disableBackground: true,
            disableQuestionMark: true,
        }
    );

    return <Card style={{height: "100%"}} ref={ref} dangerouslySetInnerHTML={{__html: plotData }}/>
}