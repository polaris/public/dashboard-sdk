import React from "react";
import PolarisBaseCard from "./PolarisBaseCard";
import { FontAwesomeIcon, solid } from "@fortawesome/react-fontawesome";
import { faExclamation, faExclamationCircle, faStar, faThumbsDown, faThumbsUp } from "@fortawesome/free-solid-svg-icons";
import PolarisLineChart from "./PolarisLineChart";
import PolarisBarChart from "./PolarisBarChart";
import PolarisStarModal from "../ratings/PolarisStarModal";
import PolarisTextRatingModal from "../ratings/PolarisTextRatingModal";
import PolarisGuageChart from "./PolarisGuageChart";
import PolarisFormCard from "./PolarisFormCard";

export default class PolarisBaseWidget extends React.Component {

    constructor(props) {
        super(props);
        this.state = {showStarRatingModal: false, showTextRatingModal: false};
      }

    render() {
        console.log(this.props)
        return <div style={{height: "100%"}} className={this.props.isResizable || this.props.isDraggable ? "db-shaking": ""} >
            {!this.props.isDraggable && !this.props.isResizable && this.props.config.feedback?.mode == "thumbs" ?
            <div className="d-flex" style={{position:"absolute", zIndex: 10, right: "5px", color: this.props.config.feedback?.color == "light" ? "#fff": "#000"}}>
                <FontAwesomeIcon icon={faThumbsDown} className="m-1" onClick={(evt) => { evt.preventDefault(); alert("test") }} />
                <FontAwesomeIcon icon={faThumbsUp} className="m-1" onClick={(evt) => { evt.preventDefault() }} /> 
            </div> : null }
            {!this.props.isDraggable && !this.props.isResizable && this.props.config.feedback?.mode == "star" ?
            <div className="d-flex" style={{position:"absolute", zIndex: 10, right: "5px", color: this.props.config.feedback?.color == "light" ? "#fff": "#000"}}>
                <FontAwesomeIcon icon={faStar} className="m-1" onClick={(evt) => { evt.preventDefault(); this.setState({ showStarRatingModal: true }) }} /> 
            </div>: null}

            {!this.props.isDraggable && !this.props.isResizable && this.props.config.feedback?.mode == "text" ?
            <div className="d-flex" style={{position:"absolute", zIndex: 10, right: "5px", color: this.props.config.feedback?.color == "light" ? "#fff": "#000"}}>
                <FontAwesomeIcon icon={faExclamationCircle} className="m-1" onClick={(evt) => { evt.preventDefault(); this.setState({ showTextRatingModal: true }) }} /> 
            </div>: null}
        {this.props.config?.type === "card" ? <PolarisBaseCard parameters={this.props.config?.parameters}/> : null}
        {this.props.config?.type === "lineChart" ? <PolarisLineChart parameters={this.props.config?.parameters} isEdit={this.props.isResizable || this.props.isDraggable}/> : null}
        {this.props.config?.type === "barChart" ? <PolarisBarChart parameters={this.props.config?.parameters} isEdit={this.props.isResizable || this.props.isDraggable}/> : null}
        {this.props.config?.type === "guageChart" ? <PolarisGuageChart parameters={this.props.config?.parameters} isEdit={this.props.isResizable || this.props.isDraggable}/> : null}
        {this.props.config?.type === "form" ? <PolarisFormCard parameters={this.props.config?.parameters} isEdit={this.props.isResizable || this.props.isDraggable}/> : null}
        <PolarisStarModal onHide={() => this.setState({ showStarRatingModal: false})} show={this.state.showStarRatingModal} configuration={this.props.config.feedback}/>
        <PolarisTextRatingModal  onHide={() => this.setState({ showTextRatingModal: false})} show={this.state.showTextRatingModal} configuration={this.props.config.feedback} />
        </div>
    }
}