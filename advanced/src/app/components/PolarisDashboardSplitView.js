import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import PolarisDashboard from "./PolarisDashboard";
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs';
import SyntaxHighlighter from "react-syntax-highlighter/dist/esm/default-highlight";

export default function PolarisDashboardSplitView({config, title, hideConfig = false, height = "auto"}) 
{

  const dashboardConfig = hideConfig ? "// Die Konfiguration ist ausgeblendet" :  'let dashboardConfig= ' + JSON.stringify(config, null, "\t") + ";";

    return <Container fluid style={{height: height}}>
        <Row>
            <h3>{title}</h3>
        </Row>
        <Row>

        <Col xs={6}>
                <PolarisDashboard config={config}/>
                </Col>
        <Col xs={6}>
        <SyntaxHighlighter language="javascript" style={docco}>
            {dashboardConfig}
            </SyntaxHighlighter>
        </Col>
        </Row>
    </Container>
}