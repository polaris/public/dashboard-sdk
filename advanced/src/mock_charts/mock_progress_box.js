import { GridWidget } from "@polaris/dashboard-sdk";
import { BoxPlotWidget } from "@polaris/dashboard-sdk";

const data = [
    {
        type: "boxplot",
        name: "Block 1",
        options: {
            showLegend: false,
        },
        data:
            {
                min: 0,
                max: 100,
                q1: 34.8,
                median: 35,
                q3: 35.2,
            },
    },
    {
        type: "boxplot",
        name: "Block 2",
        options: {
            showLegend: false,
        },
        data:
            {
                min: 0,
                max: 100,
                q1: 67.8,
                median: 68,
                q3: 68.2,
            },
    },
    {
        type: "boxplot",
        name: "Block 3",
        options: {
            showLegend: false,
        },
        data:
            {
                min: 0,
                max: 100,
                q1: 64.8,
                median: 65,
                q3: 65.2,
            },
    }
]


export function mockProgressBox(onShowDesc) {
    return { "box-progress-widget" :
            new GridWidget(
                "Kursfortschritt",
                "Fortschritt in den einzelnen Kursblöcken",
                data,
                {
                    direction: "column",
                    onShowDesc: onShowDesc
                })
    }
}
