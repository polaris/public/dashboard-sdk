const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require("path");

module.exports = {
    entry: "./src/index.js",
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "bundle.js",
        assetModuleFilename: 'assets/[hash][ext][query]'
    },
    devtool: 'eval-source-map',

    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: "babel-loader",
                    options: {
                        presets: ["@babel/preset-env", "@babel/preset-react"],
                        plugins: ["@babel/plugin-proposal-object-rest-spread"],
                    },
                },
            },

            {
                test: /\.(scss|css)$/,
                use: ['style-loader', 'css-loader', 'sass-loader'],
            },

            {
                test: /\.(gif|jpeg|jpg|png|svg|webp)$/,
                type: 'asset'
            },

            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[path]/[name].[ext]",
                            include: [/fonts/],
                        },
                    },
                ],
            },
        ],
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: "RUB Dashboard",
            template: "src/index.html",
            inject: false,
        }),
    ],
    devServer: {
        static: {
            directory: path.join(__dirname, ""),
        },
        host: "0.0.0.0",
        port: 8005,
        allowedHosts: "all",
        client: {
            overlay: {
                errors: true,
            },
            progress: true,
        },
        historyApiFallback: { index: "/", disableDotRule: true },
    },
};
